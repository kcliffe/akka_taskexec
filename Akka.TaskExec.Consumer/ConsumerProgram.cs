﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using Akka.Configuration;
using Akka.TaskExec.Lib.Hosting;

namespace Akka.TaskExec.Consumer
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var config = ConfigurationFactory.ParseString(@"
				akka {    
					stdout-loglevel = DEBUG
				    loglevel = DEBUG
				    log-config-on-start = on     
				    actor {                
				        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
				    }
					remote {
				        helios.tcp {
				            port = 8090
				            hostname = localhost
				        }
				    }
				}
			");

			using (var actorSystem = ActorSystem.Create ("client", config)) 
			{
				string clusterManagerAddress = "akka.tcp://producer@localhost:8080/user/conducter/public";

				var workerHost = new WorkerHost (actorSystem, clusterManagerAddress);
				workerHost.Start (new List<string> { "Ext_1" });

				Console.ReadLine ();
			}
		}
	}
}
