﻿using System;
using Akka.Configuration;
using Akka.Actor;
using Akka.TaskExec.Lib.Messages;
using Akka.TaskExec.Lib.Actors.Producer;
using Akka.TaskExec.Lib.Actors;
using Akka.TaskExec.Lib.Management;
using Akka.TaskExec.Storage;

namespace Akka.TaskExec
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var config = ConfigurationFactory.ParseString(@"
				akka {  
					stdout-loglevel = DEBUG
				    loglevel = DEBUG
				    log-config-on-start = off 
					loggers = [""Akka.Logger.NLog.NLogLogger, Akka.Logger.NLog""]  
				    actor {                
				        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
				    }
					remote {
				        helios.tcp {
				            port = 8080
				            hostname = localhost
				        }
				    }
				}
			");
					
			var actorSystem = ActorSystem.Create ("producer", config);

			var taskManager = new TaskManager(new MemoryStorage(), new System.IO.Abstractions.FileSystem());
            var taskAlloc = new TaskAllocator(taskManager);

            // Conductor needs both (alloc + mgr), and TaskAlloc has a Mgr ref
            // This would look way less weird with IOC
			var props = Props.Create<Conducter>(taskAlloc, taskManager);
			var conducter = actorSystem.ActorOf(props, "conducter");
		
			while (Console.ReadLine() != "q")
			{
				conducter.Tell(new NewJobNotificationMsg("manifests/multi.json"));
			}

			actorSystem.AwaitTermination();
		}
	}

	public class Test : BaseReceiveActor
	{
		public Test ()
		{
			Receive<RegisterWorkerMsg> (msg => Console.WriteLine ("Yup"));
		}
	}
}
