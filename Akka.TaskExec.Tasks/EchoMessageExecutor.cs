﻿using System;
using Akka.TaskExec.Lib.Execution;
using Akka.TaskExec.Lib.Management;
using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Tasks
{
	/// <summary>
	/// Echo a string to the console
	/// </summary>
	public class EchoMessageExecutor : BaseTaskExecutor
	{
		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
		public EchoMessageExecutor(Signal signal, IManagementNotification mn)
			: base(signal, mn)
		{			
		}

		public override void Execute(TaskDefinition taskDefinition)
		{
			// TODO. Behavior when this guy explodes (seems to die silently). check try catch above us?

			var message = taskDefinition.GetString("Msg");        // ick. in most ways strongly typed was better    
			Console.WriteLine ("Echo: {0}", message);

			// note default WorkUnits value of 1 should indicate we've started (and finished :-0)
			Progress.Update(taskDefinition.Id, taskDefinition.Name);                         
		}
	}
}

