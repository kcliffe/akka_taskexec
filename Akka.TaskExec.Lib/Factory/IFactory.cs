﻿using Akka.TaskExec.Lib.Execution;

namespace Akka.TaskExec.Lib.Factory
{
	public interface IFactory
	{
		IInvokeTasks GetTaskExecutor ();
	}

	public class Factory : IFactory
	{
	    readonly IInvokeTasks _taskInvoker;

		public Factory (IInvokeTasks taskInvoker)
		{
			_taskInvoker = taskInvoker;
		}

		public IInvokeTasks GetTaskExecutor()
		{
			return _taskInvoker;
		}
	}
}

