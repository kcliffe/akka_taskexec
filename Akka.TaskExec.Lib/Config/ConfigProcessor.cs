﻿using System.Collections.Generic;
using NLog;

namespace Akka.TaskExec.Lib.Config
{
	public interface IVariableResolver
	{
		bool Handle (string wildcard, out string val);
	}

	public class ConfigurationProcessor
	{
		private readonly List<IVariableResolver> _wildcardResolvers;
		private static readonly NLog.Logger Logger = LogManager.GetCurrentClassLogger();

		public ConfigurationProcessor (List<IVariableResolver> wildcardResolvers)
		{
			_wildcardResolvers = wildcardResolvers;
		}

		public void ResolveParams(IEnumerable<IDictionary<string, string>> allParams)
		{
			foreach(var dict in allParams)
			{
				foreach (var keyvalue in dict) 
				{
					Logger.Debug ("Attempting to resolve param {0}", keyvalue.Value);
					var resolved = ResolveVariableExpression (keyvalue.Value);
					if (!string.IsNullOrEmpty (resolved))
						dict [keyvalue.Key] = resolved;
				}
			}
		}

		// currentl only used in tests
		public string ResolveVariableExpression (string wildcardExpr)
		{
			var resolved = false;

			// throw if we can't resolve
			foreach (var resolver in _wildcardResolvers) {
				string resolvedExpr;
				resolved = resolver.Handle (wildcardExpr, out resolvedExpr);
				if (resolved) {
					return resolvedExpr;
				}
			}

			return string.Empty;
		}
	}
}

