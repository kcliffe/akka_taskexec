﻿using System;

namespace Akka.TaskExec.Lib
{
	public class ConsoleOut
	{
		public static void Write (string s, params string[] args)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine (s, args);
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}

