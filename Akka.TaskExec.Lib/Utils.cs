﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Akka.TaskExec.Lib
{
	public static class Utils
	{
		public static string GetRootedPath(string relativePath)
		{
			var path = Assembly.GetCallingAssembly().Location;
			var loc = path.IndexOf ("TaskExec", StringComparison.CurrentCultureIgnoreCase) 
				+ "TaskExec".Length;

			var root = path.Substring (0, loc);
			return Path.Combine (root, relativePath);
		}
	}

	public static class LinqExt
	{
		public static bool AnySafe<TSource>(this IEnumerable<TSource> source)
		{
		    return source != null && source.Any ();
		}

	    public static IEnumerable<TSource> DistinctBy<TSource, TKey>
		(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			var seenKeys = new HashSet<TKey>();
			foreach (TSource element in source)
			{
				if (seenKeys.Add(keySelector(element)))
				{
					yield return element;
				}
			}
		}
	}

	public class TypeEnumerator
	{
		// private static readonly Logger Logger = LogManager.GetLogger("dev_debug");

		public static IEnumerable<Type> EnumerateTypes(string path, string fileSpec, Func<Type, bool> filter)
		{
			foreach (var type in EnumerateTypes(path, fileSpec)) 
			{
				if (filter (type))
					yield return type;
			}
		}

		private static IEnumerable<Type> EnumerateTypes(string path, string fileSpec)
		{
			var folderName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var assemblies = Directory.EnumerateFiles(folderName, fileSpec, SearchOption.TopDirectoryOnly);

			foreach (var f in assemblies)
			{
				// Logger.Debug(string.Format("Enumerating assembly {0} for module discovery", f));

				var assembly = Assembly.LoadFrom(f);
				foreach (var type in assembly.GetTypes())
				{
					// Logger.Debug(string.Format("Enumerating type {0} for module discovery", type.Name));
					yield return type;
				}
			}
		}

	}
}

