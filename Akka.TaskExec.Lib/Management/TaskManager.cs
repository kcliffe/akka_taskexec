﻿using System.Linq;
using System.Collections.Generic;
using NLog;
using Akka.TaskExec.Lib.Exceptions;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Messages;
using Akka.TaskExec.Lib.Storage;
using Akka.TaskExec.Lib.Config;
using System.IO.Abstractions;

namespace Akka.TaskExec.Lib.Management
{
	/// <summary>
	/// Manage the state transitions that occur for manifests / jobs / tasks when 
	/// a) new manifests are created
	/// b) tasks are scheduled for execution
	/// c) tasks complete execution
	/// d) task progress updates are received        
	/// </summary>
	public class TaskManager : IManageTasks
	{
		private static readonly NLog.Logger Logger = LogManager.GetLogger(typeof(TaskManager).Name);

		private readonly ISerializer _jsonSerializer;
		private readonly IFileSystem _fileSystem;
		private readonly IPersistJobs _jobPersister;

		public IEnumerable<TaskDefinition> CurrentTasks {
			get 
			{
                return _jobPersister
                    .GetAll()
                    .SelectMany(m => m.Tasks)
                    .ToList();
			}
		}

		public TaskManager (IPersistJobs manifestPersister, IFileSystem fileSystem)
		{
			_jobPersister = manifestPersister;
			_fileSystem = fileSystem;
			_jsonSerializer = new JsonSerializer ();
		}

		/// <summary>
		/// Deserialize the manifest, validate it, perform any additional works (e.g resolve any special  wildcards 
		/// and finally create a persistent version of the manifest which can be scheduled for execution.
		/// </summary>
		/// <param name="manifestPath">Manifest path. When a new manifest notification is received 
		/// it supplies the path to the manifest on disk.</param>
		public void CreateRuntimeManifest (string manifestPath)
		{
            // todo -> read the file, locate the SINGLE job we are able to run
		    var rootedPath = Utils.GetRootedPath(manifestPath);

			// the notification only contains the file path, so read the manifest file contents
            var manifestContent = _fileSystem.File.ReadAllText(rootedPath);
			var manifest = _jsonSerializer.Deserialize<List<JobDefinition>>(manifestContent);
            var job = manifest[0];

			// process any special elements in the manifest
			var configurationProcessor = new ConfigurationProcessor(new List<IVariableResolver> {
				new SpecialFolderVariableResolver(),
				new EnvironmentVariableVariableResolver()
			});

			// we should no longer be doing this (let persister do it)
			job.AssignUniqueIdToSubTasks (); // will be redundant for non memory (pk or auto-gen ids used)
            job.CopyJobNameToSubTasks();
            job.MergeJobLevelDependenciesToSubTasks();
            job.MergeJobLevelParamsToSubTasks();

			try 
			{
				configurationProcessor.ResolveParams(job.Tasks.Select(t => t.Params));
				_jobPersister.Put(job);	
			}
			catch (InvalidManifestException ex) 
			{
				Logger.Debug (ex, "The manifest {0} is invalid...", job.Id);
			}	    
		}

		public void SetTaskExecuting (string taskId)
		{
			var taskDef = CurrentTasks.First (t => t.Id == taskId);
			SetTaskStatus(taskDef, ExecutionState.Executing);
		}

		public void SetTaskComplete (string taskId)
		{
			var taskDef = CurrentTasks.First (t => t.Id == taskId);
			SetTaskStatus(taskDef, ExecutionState.Complete);

			// if all complete, archive...
			if (CurrentTasks
				.Where (j => j.ManifestId == taskDef.ManifestId)
				.All(t => t.ExecutionState == ExecutionState.Complete))
			{
				// TODO. Complete manifest
			}
		}

		public double SetTaskProgress(TaskDefinition taskDef, int workUnits, bool incrementWork, WorkUnitType workUnitType)
		{
			double progressPercent;

			if (workUnitType == WorkUnitType.Relative) 
			{
				taskDef.TotalWork = taskDef.TotalWork ?? workUnits;

				// we increment work whenever we have a task that has sent a progress update
				// and isn't using the progress message to "initialize" progress tracking.
				if (incrementWork) {
					taskDef.WorkCompleted++;
				}
				progressPercent = (taskDef.WorkCompleted / taskDef.TotalWork.Value) * 100;
			} 
			else 
			{
				progressPercent = workUnits;
			}

			_jobPersister.UpdateTask (taskDef);			
			return progressPercent;
		}

		private void SetTaskStatus(TaskDefinition taskDef, ExecutionState state)
		{
			taskDef.ExecutionState = state;
			_jobPersister.UpdateTask (taskDef);		
		}

        //public void SetManifestStatus(string manifestId, ExecutionState executionState)
        //{
        //    var manifest = _jobPersister.GetAll().SingleOrDefault (m=> m.Id == manifestId);
        //    if (manifest == null)
        //        throw new KeyNotFoundException ("Manifest {0} does not exist");

        //    manifest.ExecutionState = ExecutionState.Cancelled;
        //    manifest.AllTasks().ToList().ForEach(t => {
        //        t.ExecutionState = ExecutionState.Cancelled;
        //    });

        //    _jobPersister.Put(manifest);	
        //}

		public void SetTaskCheckpointInfo(string taskId, string serializedRestartInfo)
		{
			var taskDef = QueryTaskById (taskId);
			taskDef.RestartInfo = serializedRestartInfo;
			_jobPersister.UpdateTask (taskDef);

			Logger.Debug("Sort of persisting checkpoint info for task {0}", taskId);
		}

		public IEnumerable<TaskDefinition> QueryPendingTasks ()
		{
			return CurrentTasks.Where(t => t.ExecutionState == ExecutionState.Pending).ToList();
		}

		public bool QueryManifestIsComplete(string manifestId) 
		{
			return CurrentTasks.All(t => t.ManifestId == manifestId && t.ExecutionState == ExecutionState.Complete);
		}

		public IEnumerable<TaskDefinition> QueryExecutingTasksForJob(string jobName) 
		{
			return QueryTasksWithStateForJob(jobName, ExecutionState.Executing);
		}

		public TaskDefinition QueryTaskById (string taskId)
		{
			return CurrentTasks.First (t => t.Id == taskId);
		}

		// yuck
		public IEnumerable<JobDefinition> QueryExecutingJobs ()
		{
			var executingJobs = CurrentTasks.Where(t => t.ExecutionState == ExecutionState.Executing)
				.Select(t => t.JobName)
				.Distinct();

			return _jobPersister.GetAll().Where(j => executingJobs.Contains(j.Name));
		}

		public bool QueryJobIsComplete(string jobName) 
		{
			return CurrentTasks.Where(t => t.JobName == jobName).All(t => t.ExecutionState == ExecutionState.Complete);
		}

		public string QueryJobNameByTaskId (string taskId)
		{
			return CurrentTasks.First (x => x.Id == taskId).JobName;
		}

		private IEnumerable<TaskDefinition> QueryTasksWithStateForJob(string jobName,
			ExecutionState state) 
		{
			var executingTasks = CurrentTasks.Where(x => x.ExecutionState == state	&& x.JobName == jobName);
			return executingTasks;
		}

        //private ManifestDefinition QueryOwningManifest (string manifestId)
        //{
        //    return _jobPersister.GetAll().First(m => m.Id == manifestId);
        //}
	}
}

