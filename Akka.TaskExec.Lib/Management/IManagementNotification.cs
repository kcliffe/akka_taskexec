﻿using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Management
{
	public interface IManagementNotification
	{
		void Update (string taskId, string taskName, int workUnits = 1, bool initialiseOnly = false, 
			WorkUnitType workUnitType = WorkUnitType.Relative);

		// void RegisterWorker (WorkerRegistration workerRegistration);

		void CreateCheckpoint (string taskId, dynamic restartInfoValue);
	}
}

