﻿using System.Collections.Generic;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Management
{
	public interface IManageTasks
	{        
		void CreateRuntimeManifest (string manifestPath);
		//void SetManifestStatus (string manifestName, ExecutionState executionState);
		IEnumerable<TaskDefinition> QueryPendingTasks ();
		TaskDefinition QueryTaskById (string taskId);
		IEnumerable<JobDefinition> QueryExecutingJobs ();
		IEnumerable<TaskDefinition> QueryExecutingTasksForJob(string jobName);
		string QueryJobNameByTaskId (string taskId);
		bool QueryJobIsComplete(string jobName);
		//bool QueryManifestIsComplete (string manifestId);  
		void SetTaskExecuting (string taskId);
		void SetTaskComplete (string taskId);
		double SetTaskProgress (TaskDefinition taskDef, int workUnits, bool incrementWork, WorkUnitType workUnitType);
		void SetTaskCheckpointInfo(string taskId, string restartInfoValue); 
	}
}