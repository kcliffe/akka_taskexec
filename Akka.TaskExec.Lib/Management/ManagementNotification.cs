﻿using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Management
{
	public class ManagementNotification : IManagementNotification
	{
		public void Update (string taskId, string taskName, int workUnits = 1, bool initialiseOnly = false, 
			WorkUnitType workUnitType = WorkUnitType.Relative)
		{
		}

		public void CreateCheckpoint (string taskId, dynamic restartInfoValue)
		{
		}
	}
}

