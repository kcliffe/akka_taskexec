using System.Collections.Generic;

namespace Akka.TaskExec.Storage.Schema
{
	public interface IPersistableManifest
	{
        string Id { get; set; }
		string Name {get;set;}
		List<IPersistableJob> Jobs { get; set;}	    
	}
}

