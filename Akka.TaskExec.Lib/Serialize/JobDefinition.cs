﻿using System.Collections.Generic;
using System.Linq;

namespace Akka.TaskExec.Lib.Serialize
{
    /// <summary>
    /// A job is currently a simple list of tasks
    /// </summary>
    public class JobDefinition : JobBase
    {
        private int _taskId;

        public List<TaskDefinition> Tasks { get; set; }

        public bool IsComplete()
        {
            return Tasks.All(t => t.ExecutionState == ExecutionState.Complete);
        }

        public void AssignUniqueIdToSubTasks()
        {
			Tasks.ForEach(t => 
			{
				t.Id = System.Threading.Interlocked.Increment(ref _taskId).ToString();
			});
        }

        //public void CopyManifestIdToSubTasks()
        //{
        //    Jobs.ForEach(j => j.Tasks.ForEach(t => t.ManifestId = Id));
        //}

        public void CopyJobNameToSubTasks()
        {
            Tasks.ForEach(t => t.JobName = Name);
        }

        public void MergeJobLevelDependenciesToSubTasks()
        {
            if (!HasDependecies) return;
            Tasks.ForEach(t => t.MergeDependencies(Depends));
        }

		public void MergeJobLevelParamsToSubTasks()
		{
            if (!Params.Any()) return;
            Tasks.ForEach(t => t.MergeParams(Params));
		}
    }
}