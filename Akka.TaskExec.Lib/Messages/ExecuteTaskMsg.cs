﻿using Akka.TaskExec.Lib.Serialize;
using Newtonsoft.Json;

namespace Akka.TaskExec.Lib.Messages
{
	public class ExecuteTaskMsg
	{
		[JsonProperty]
		public TaskDefinition TaskDefinition { get; private set; }

		public ExecuteTaskMsg (TaskDefinition td)
		{
			TaskDefinition = td;
		}
	}
}

