﻿using Newtonsoft.Json;
using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Lib.Messages
{
	public class TaskCompleteMessage
	{
		[JsonProperty]
		public TaskDefinition TaskDef { get; private set; }

		public TaskCompleteMessage (TaskDefinition taskDef)
		{
			TaskDef = taskDef;
		}
	}
}

