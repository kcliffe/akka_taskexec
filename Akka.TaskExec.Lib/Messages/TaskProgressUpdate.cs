﻿namespace Akka.TaskExec.Lib.Messages
{
	/// <summary>
	/// Send when a unit of work that is part of a task completes
	/// 
	/// TODO. Immutable
	/// </summary>
	public class TaskProgressUpdateMsg
	{       
		/// <summary>
		/// If the task is broken into worker tasks, this is the total count
		/// which allows us to calculate progress
		/// </summary>
		/// <value>The work units.</value>
		private int _workUnits = 1;
		public int WorkUnits 
		{ 
			get { return _workUnits; }	
			set { _workUnits = value; } 
		}

		private bool _initialiseOnly;
		public bool InitialiseOnly 
		{ 
			get { return _initialiseOnly; }	
			set { _initialiseOnly = value; } 
		}

		private WorkUnitType _workUnitType;
		public WorkUnitType WorkUnitType 
		{ 
			get { return _workUnitType; }	
			set { _workUnitType = value; } 
		}
	}

	public enum WorkUnitType 
	{
		Relative,
		Absolute
	}
}

