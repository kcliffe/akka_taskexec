﻿using Newtonsoft.Json;

namespace Akka.TaskExec.Lib.Messages
{
	public class BadWorkerMessage
	{
		[JsonProperty]
		public string WorkerId { get; private set; } 

		public BadWorkerMessage(string workerId)
		{
			WorkerId = workerId;
		}
	}
}

