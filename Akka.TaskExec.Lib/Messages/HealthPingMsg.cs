﻿using Newtonsoft.Json;

namespace Akka.TaskExec.Lib.Messages
{
	public class HealthPingMsg
	{
		[JsonProperty]
		public string WorkerId { get; private set; }

		public HealthPingMsg (string workerId)
		{
			WorkerId = workerId;
		}
	}
}

