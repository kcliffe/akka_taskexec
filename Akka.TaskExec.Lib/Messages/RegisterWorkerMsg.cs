﻿using Newtonsoft.Json;

namespace Akka.TaskExec.Lib.Messages
{
	public class RegisterWorkerMsg
	{
		[JsonProperty]
		public string WorkerId { get; private set; }
	
		public RegisterWorkerMsg (string workerId)
		{
			WorkerId = workerId;
		}
	}
}

