﻿using System;

namespace Akka.TaskExec.Lib.Exceptions
{
	[Serializable]
	public class TaskExecutorResolverException : NonTransientException
	{
		public TaskExecutorResolverException (string message) : base(message)
		{
		}

		public TaskExecutorResolverException (string message, Exception ex) : base(message, ex)
		{
		}
	}
}

