﻿using System;

namespace Akka.TaskExec.Lib.Exceptions
{
    [Serializable]
	public class InvalidManifestException : Exception
	{
		public InvalidManifestException (string message) : base(message)
		{
		}

		public InvalidManifestException (string message, Exception ex) : base(message, ex)
		{
		}
	}
}

