﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Actors.Consumer
{
	/// <summary>
	/// You get this stuff for free in Akka clusters. Pretty simple impl though.
	/// </summary>
	public class HealthMonitor : BaseReceiveActor
	{
	    readonly TimeSpan _checkHealthEvery = TimeSpan.FromSeconds(15);
	    readonly TimeSpan _badWorkerInterval = TimeSpan.FromSeconds(30);

		// only "external" workers registerand send HealthPings (in process workers
		// don't generally require health checks - if they've gone down, so has the process...
	    readonly Dictionary<string, DateTimeOffset> _lastContacts;
	    readonly ICancelable _cancelSchedule;

		public HealthMonitor ()
		{
			_lastContacts = new Dictionary<string, DateTimeOffset>();

			Receive<HealthPingMsg> (msg => HealthPingReceived(msg));
			Receive<HealthCheckMessage> (msg => CheckWorkerHealth());

			_cancelSchedule = Context.System.Scheduler
				.ScheduleTellRepeatedlyCancelable (_checkHealthEvery, _checkHealthEvery, Self, new HealthCheckMessage(), Self);
		}

		private void HealthPingReceived(HealthPingMsg msg)
		{
			_lastContacts [msg.WorkerId] = DateTime.UtcNow;
		}

		private void CheckWorkerHealth()
		{
			foreach (var kv in _lastContacts.Keys) {
				if ((DateTimeOffset.UtcNow - _lastContacts [kv]) > _badWorkerInterval) {
					Context.Parent.Tell (new BadWorkerMessage (kv));
				}
			}
		}

		public override void AroundPostStop ()
		{
			base.AroundPostStop ();

			_cancelSchedule.CancelIfNotNull ();
		}

		public class HealthCheckMessage
		{
		}
	}
}

