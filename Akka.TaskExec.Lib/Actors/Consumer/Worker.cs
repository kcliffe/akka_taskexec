﻿using System;
using Akka.Actor;
using Akka.TaskExec.Lib.Factory;
using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Actors.Consumer
{
	/// <summary>
	/// Manage execution of a task
	/// </summary>
	public class Worker : BaseReceiveActor
	{
		IActorRef _clusterManager;
	    readonly string _clusterManagerAddress;
	    readonly IFactory _factory;
	    readonly string _workerId;

		/// <summary>
		/// Construct a remote (out of process) worker
		/// </summary>
		/// <param name="theFactory">The factory.</param>
		/// <param name="workerId">Worker identifier.</param>
		/// <param name="clusterManagerAddress">Cluster manager address.</param>
		public Worker(IFactory theFactory, string workerId, string clusterManagerAddress)
		{
			_workerId = workerId;
			_factory = theFactory;
			_clusterManagerAddress = clusterManagerAddress;

			Log.Debug ("(ext) Worker {0} created. Registering...", _workerId);
			Become (Registering);
		}
	
		/// <summary>
		/// Construct an "in process" worker. We may drop this
		/// </summary>
		/// <param name="theFactory">The factory.</param>
		/// <param name="workerId">Worker identifier.</param>
		public Worker(IFactory theFactory, string workerId)
		{
			_factory = theFactory;
			_workerId = workerId;
			_clusterManager = Context.Parent;

			Become (ReadyToProcess);
		}

		/// <summary>
		/// State
		/// </summary>
		private void Registering()
		{
			try{
				_clusterManager = Context
					.ActorSelection (_clusterManagerAddress)
					.ResolveOne(TimeSpan.FromSeconds(1))
					.Result;
			}
			catch(ActorNotFoundException) {
				Log.Debug ("SHIT!!! Unable to reach cluster manager");
				throw;
			}

			Receive<WorkerRegisteredMsg>(msg => { 
				Log.Debug("{0} registered successfully", _workerId);
				Context.SetReceiveTimeout(null);
				Become(ReadyToProcess);
			});

			Context.SetReceiveTimeout (TimeSpan.FromSeconds (5));
			_clusterManager.Tell (new RegisterWorkerMsg (_workerId));

			Log.Debug ("{0} registration sent to {1}", _workerId, _clusterManagerAddress);
		}
	
		/// <summary>
		/// State
		/// </summary>
		private void ReadyToProcess()
		{
			Log.Debug("Worker {0} ready to process", _workerId);

			Receive<ExecuteTaskMsg> (td => ExecuteTask (td));
		}

		/// <summary>
		/// Executes the task.
		/// </summary>
		/// <param name="msg">Message.</param>
		private void ExecuteTask(ExecuteTaskMsg msg)
		{
			Log.Debug("Executing task {0}", msg.TaskDefinition.Name);

			var exec = _factory.GetTaskExecutor ();
			exec.Execute(msg.TaskDefinition);
			
			_clusterManager.Tell (new TaskCompleteMessage(msg.TaskDefinition));
		}
	}
}

