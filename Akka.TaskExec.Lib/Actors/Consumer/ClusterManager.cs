using System.Collections.Generic;
using Akka.Actor;
using Akka.TaskExec.Lib.Messages;

namespace Akka.TaskExec.Lib.Actors.Consumer
{
    /// <summary>
    /// This guy manages registered workers. Delivery and health.
    /// Akka.Cluster is the thing to use once grokked.
    /// </summary>
	public class ClusterManager : BaseReceiveActor
	{
        readonly IActorRef _producer;
        readonly List<IActorRef> _workers;	// todo track retries 
		int _nextWorker;

		public ClusterManager()
		{
			_producer = Context.Parent;
			_workers = new List<IActorRef>(); 

			Log.Info("ClusterManager created on {0}", Self.Path);
		}

		public override void AroundPreStart()
		{
			base.AroundPreStart ();

			var healthMonProps = Props.Create<HealthMonitor>();
			Context.ActorOf (healthMonProps, "HealthMon");

			Become (ReadyToProcess);
		}

		/// <summary>
		/// State
		/// </summary>
		private void ReadyToProcess()
		{
			Receive<RegisterWorkerMsg> (msg => RegisterWorker(msg, Sender));
			Receive<ExecuteTaskMsg> (td => Distribute (td));
			Receive<BadWorkerMessage> (msg => HandleBadWorker (msg));
			Receive<TaskCompleteMessage> (msg => _producer.Forward (msg));
			Receive<Terminated> (msg => WhenWorkerTerminates (msg));
		}
	    
        // This method is PUSH. And it doesn't ask workers if they're busy or not :-)
        // However, workers are actors, so the messages will simply end up in their
        // inboxes util they're ready to process them.
        // It's not really a totally FAIR policy since workers doing heavy tasks
        // might end up with work pending while others are available.
        // It's probably a good idea to go to a PULL based approach where whoever is available
        // gets given work...
		private void Distribute(ExecuteTaskMsg msg)
		{
			if (!_workers.AnySafe ())
                // KC. So..... we don't appear to be queing here
                // We need to queue messages until workers are avail and if no workers avail 
                // and we've queued X messages, we throw back to caller...
                // We'd need this approach for PULL anyway
				return;
		
			Log.Debug ("work for {0} of {1} ({2})", _nextWorker, _workers.Count, _workers[_nextWorker].Path);

			_workers [_nextWorker].Tell (msg);
			_nextWorker = (_nextWorker >= _workers.Count) ? 0 : _nextWorker++;
		}

		private void HandleBadWorker(BadWorkerMessage msg)
		{
			Log.Info("Bad! BTW. You get this for free in Akka clustering....");
		}

		private void RegisterWorker(RegisterWorkerMsg msg, IActorRef worker)
		{
			Log.Debug ("Received worker reg msg for {0} ({1})", msg.WorkerId, worker.Path);

			// IActorRef should be equatable
			if (!_workers.Contains (worker)) {
				_workers.Add (worker);
				worker.Tell (new WorkerRegisteredMsg ());

				// register for DeathWatch. We'll be notified when the remote actor terminates
				Context.Watch (worker);

				Log.Info ("Registered worker {0}", msg.WorkerId);
			} 
			else 
			{
				ConsoleOut.Write ("Somehow we've managed to have a worker re-reg???");
			}
		}

		private void WhenWorkerTerminates(Terminated msg)
		{
		    if (!_workers.Contains(msg.ActorRef)) return;

			// we check, because workers can take a while to terminate
			// and we can actually receive the registration first if the process is restarted
			if (_workers.Contains(msg.ActorRef))
			{
				Log.Info("Actor terminated. Removing from worker list.");
				_workers.Remove (msg.ActorRef);
			}
		}
	}
}
	