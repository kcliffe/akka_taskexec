﻿using System.Runtime.InteropServices;
using Akka.Actor;
using Akka.Event;

namespace Akka.TaskExec.Lib.Actors
{
    [ComVisible(false)]
	public class BaseReceiveActor : ReceiveActor
	{
		protected readonly ILoggingAdapter Log = Logging.GetLogger(Context);		
	}
}

