﻿using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using Akka.TaskExec.Lib.Actors.Consumer;
using Akka.TaskExec.Lib.Messages;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Management;

namespace Akka.TaskExec.Lib.Actors.Producer
{
	public class Conducter : BaseReceiveActor
	{
		const string DEFAULT_CLUSTER = "DEFAULT";

		readonly Dictionary<string, IActorRef> _clusterManagers;
		readonly TaskAllocator _taskAllocator;
		readonly TaskManager _taskManager;

		public Conducter (TaskAllocator taskAllocator, TaskManager taskManager)
		{
			_clusterManagers = new Dictionary<string, IActorRef> ();
			_taskManager = taskManager;
			_taskAllocator = taskAllocator;

			// create a default work cluster
			var props = Props.Create<ClusterManager>();
			_clusterManagers.Add(DEFAULT_CLUSTER, Context.ActorOf(props, "public"));

			Receive<BadWorkerMessage> (msg => Log.Info(msg.WorkerId));
			Receive<NewJobNotificationMsg> (md => ManifestReceived(md));
			Receive<TaskCompleteMessage> (md => TaskComplete(md));

			Log.Info ("Conducter up");
		}

		/// <summary>
		/// Msg
		/// </summary>
		/// <param name="msg">Message.</param>
		private void ManifestReceived(NewJobNotificationMsg msg)
		{
			Log.Info(string.Format("Manifest loaded from {0}. Creating runtime manifest...", msg.ManifestPath));

			// read/parse/validate and store (as persistent XEREM classes) the incoming static manifest
			_taskManager.CreateRuntimeManifest(msg.ManifestPath); 

			// and then begin to pass the available tasks to workers / consumers
			BeginQueueingPendingTasks();
		}

		/// <summary>
		/// Msg
		/// </summary>
		/// <param name="msg">Message.</param>
		private void TaskComplete(TaskCompleteMessage msg)
		{
			var taskDef = msg.TaskDef;

			Log.Info("Task {0} complete", taskDef.Description);
			_taskManager.SetTaskComplete(taskDef.Id);
			_taskAllocator.ReleaseGlobalMutexes(taskDef);

			// remove this task from dependency list of all tasks listing it as a dependency
			var allDependentTasks = _taskManager.QueryPendingTasks().Where(t => t.IsDependentOn(taskDef.Name));
			foreach (var depTask in allDependentTasks)
			{
				Log.Debug("Removing dep on task {0} from task {1}", taskDef.Name, depTask.Name);
				depTask.RemoveDependency(taskDef.Name);
			}

			// if this task completes processing of it's parent job, remove the job from all dependant (job) tasks
			if (_taskManager.QueryJobIsComplete(taskDef.JobName))
			{
				allDependentTasks = _taskManager.QueryPendingTasks().Where(t => t.IsDependentOn(taskDef.JobName));
				foreach (var depTask in allDependentTasks)
				{
					Log.Debug("Removing dep on Job {0} from task {1}", taskDef.JobName, depTask.Name);
					depTask.RemoveDependency(taskDef.JobName);
				}
			}

			// we're done with this manifest?
			if (_taskManager.QueryManifestIsComplete(taskDef.ManifestId))
			{
				Log.Info("All tasks complete.");
				return;
			}

			// there are other tasks that may now be free to go...
			int concurrencyReason;
			var nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);
			if (nextTask != null)
				QueueTask(nextTask);
		}

		/// <summary>
		/// Triggered only when a new manifest is submitted	
		/// </summary>
		private void BeginQueueingPendingTasks()
		{
			Log.Info("Processing pending tasks");

			int concurrencyReason;
			var nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);

			// process as many jobs as we can until concurrency limits are hit.
			while (nextTask != null)
			{
				QueueTask(nextTask);
				nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);
			}
		}

		private void QueueTask(TaskDefinition taskToExecute)
		{
			if (taskToExecute == null) return;
			Log.Info("Queueing task ({1}){0}", taskToExecute.Id, taskToExecute.Name);

			if (taskToExecute.HasRoute)
			{
				_clusterManagers[taskToExecute.Route].Tell(new ExecuteTaskMsg(taskToExecute));
			}
			else
			{
				_clusterManagers[DEFAULT_CLUSTER].Tell(new ExecuteTaskMsg(taskToExecute));
			}
				
			_taskManager.SetTaskExecuting(taskToExecute.Id);
		}
	}
}

