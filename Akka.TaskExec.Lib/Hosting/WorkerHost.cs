﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Autofac;
using Autofac.Core;
using Akka.Actor;
using Akka.TaskExec.Lib.Actors.Consumer;
using Akka.TaskExec.Lib.Execution;
using Akka.TaskExec.Lib.Management;
using NLog;

namespace Akka.TaskExec.Lib.Hosting
{
	public class WorkerHost
	{
		private static readonly NLog.Logger Logger = LogManager.GetLogger(typeof(WorkerHost).Name);

		readonly ActorSystem _actorSystem;
		readonly ContainerBuilder _builder;
		readonly ITaskFactory _taskFactory;		
	    readonly string _clusterManagerAddress;
        IContainer _container;

		public WorkerHost(ActorSystem actorSystem, string clusterManagerAddress)
		{
			_actorSystem = actorSystem;
			_clusterManagerAddress = clusterManagerAddress;

			_taskFactory = new TaskFactory();
			_builder = new ContainerBuilder();
		}
			
		public void Start(List<string> workerNames)
		{
			Logger.Info("Discovering task executors");
			_taskFactory.DoExecutorDiscovery (_builder);

			Logger.Info("Discovering modules and configuring task dependencies");
			DoModuleDiscovery();
			AddSystemDependencies();

			_container = _builder.Build();

			// signal needs pre create
			var executor = new DefaultTaskInvoker((t) => 
				{ 
					var task = _container.Resolve(t, new TypedParameter(typeof(Signal), new Signal()));
					return (IAmATaskExecutor)task;
				}, _taskFactory);

			workerNames.ForEach (name => {
				// create the worker instance
				var workerProps = Props.Create<Worker> (new Factory.Factory (executor), name, _clusterManagerAddress);
				_actorSystem.ActorOf (workerProps, name);
			});
		}
			
		private void DoModuleDiscovery()
		{
			var folderName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var types = TypeEnumerator.EnumerateTypes (folderName, "Akka.TaskExec.*.dll", (t) => typeof(Autofac.Module).IsAssignableFrom(t));

			foreach (var type in types) 
			{
				Logger.Debug("Registering autofac module of type {0}", type.Name);
				var moduleInstance = (IModule)Activator.CreateInstance (type);
				_builder.RegisterModule (moduleInstance);	
			}
		}

		private void AddSystemDependencies()
		{
			_builder.RegisterType (typeof(ManagementNotification)).As<IManagementNotification> ();
		}
	}
}

