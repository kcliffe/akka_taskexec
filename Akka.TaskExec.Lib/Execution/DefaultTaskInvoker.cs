﻿using Akka.TaskExec.Lib.Exceptions;
using Akka.TaskExec.Lib.Execution.Legacy;
using Akka.TaskExec.Lib.Serialize;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Akka.TaskExec.Lib.Execution
{
	/// <summary>
	/// Manage invocations of a tasks Execute() method when a message has been dequeued.
	/// The TaskExecutor will also apply filters before locating and executing the handler.
	/// These may be useful for some scenario's eg: Don't invoke aborted / cancelled tasks.
	/// </summary>
	public class DefaultTaskInvoker : IInvokeTasks
	{
		private readonly ConcurrentDictionary<string, byte> _filteredJobs;
		private readonly ITaskFactory _taskFactory;
		private readonly Func<Type, IAmATaskExecutor> _createTaskInvokerFunc;

		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		public DefaultTaskInvoker(Func<Type, IAmATaskExecutor> createTaskInvokerFunc, ITaskFactory taskFactory)
		{
			_createTaskInvokerFunc = createTaskInvokerFunc;
			_taskFactory = taskFactory;
			_filteredJobs = new ConcurrentDictionary<string, byte> ();
		}	

		/// <summary>
		/// A message (task definition) has been received and deserialized by the transport (consumer). This method
		/// creates an instance of the "handler" and invokes its execute method passing the TaskDefinition. 
		/// Exceptions are handled by the transport.
		/// </summary>
		/// <param name="taskDefinition">Task definition.</param>
		public void Execute (TaskDefinition taskDefinition)
		{
			if (_filteredJobs.Keys.Any(j => string.CompareOrdinal(taskDefinition.JobName, j) == 0))
			{
				Logger.Debug("Execution of task {0} aborted by filter", taskDefinition.Name);
				return;
			}

			// Factory could be IOC, actually the NotificationHandler pattern should be used here...
			var taskExecutorMetadata = _taskFactory.GetTaskImplementation(taskDefinition);
			if (taskExecutorMetadata.IsLegacy)
			{
				Logger.Debug("[SOT] Execution of legacy task {0} starting", taskDefinition.Name);
				ExecuteLegacyTask(taskDefinition, taskExecutorMetadata);
				Logger.Debug("[EOT] Execution of legacy task {0} complete", taskDefinition.Name);
			}
			else
			{
				Logger.Debug("[SOT] Execution of task {0} starting", taskDefinition.Name);
				var instance = _createTaskInvokerFunc(taskExecutorMetadata.HandlerType);
				instance.Execute (taskDefinition);
				Logger.Debug("[EOT] Execution of task {0} complete", taskDefinition.Name);
			}
		}

		/// <summary>
		/// Support legacy tasks which don't take args and need the ITaskContext populated
		/// </summary>
		/// <param name="taskDefinition"></param>
		/// <param name="taskExecutorMetadata"></param>r
		private void ExecuteLegacyTask(TaskDefinition taskDefinition, TaskExecutorMetadata taskExecutorMetadata)
		{
			var originalType = taskExecutorMetadata.HandlerType;
			var legacyWrapperType = typeof(LegacyWrapperTaskExecutor);
			var instance = _createTaskInvokerFunc(legacyWrapperType);

			// TODO. Check the suspicious cast :-)
			((LegacyWrapperTaskExecutor)instance).OrginalType = originalType;
			instance.Execute(taskDefinition);
		}

		public void AddFilteredJob(string job)
		{
			Logger.Debug("Adding {0} to the filtered job list - types of this job / task will not be executed", job);
			if (string.IsNullOrEmpty(job)) throw new NonTransientException("Job name is required");

			// when to clear???
			_filteredJobs[job] = 1;
		}
	}
}

