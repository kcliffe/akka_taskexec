﻿using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Lib.Execution
{
	public interface ITaskExecutor
	{
		void ExecuteTask(TaskDefinition taskDef);
	}

	public class TaskExecutor : ITaskExecutor
	{
		public void ExecuteTask(TaskDefinition taskDef)
		{
		}
	}
}

