﻿using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Management;

namespace Akka.TaskExec.Lib.Execution
{
	/// <summary>
	/// Manage invocations of a tasks Execute() method when a message has been dequeued.
	/// The TaskExecutor will also apply filters before locating and executing the handler.
	/// These may be useful for some scenario's eg: Don't execute aborted / cancelled tasks.
	/// </summary>
	public class BaseTaskExecutor : IAmATaskExecutor
    {
        // protected static Logger Logger = LogManager.GetCurrentClassLogger();
		protected readonly IManagementNotification Progress;
		protected readonly Signal Signal;

		protected BaseTaskExecutor(Signal signal, IManagementNotification managementNotificaton)
        {
			Progress = managementNotificaton;
			Signal = signal;
        }

		public virtual void Execute(TaskDefinition task)
        {
        }       
    }
}

