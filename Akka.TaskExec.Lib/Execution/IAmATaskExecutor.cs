﻿using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Lib.Execution
{
	public interface ITaskExecutor
	{
	}

	/// <summary>
	/// Tasks which perform work implement this interface.
	/// </summary>
	public interface IAmATaskExecutor : ITaskExecutor
	{
		void Execute(TaskDefinition task);    
	}

	public interface IAmALegacyTaskExecutor : ITaskExecutor
	{
		void Execute();
		bool IsPartitioned { get; }
	}
}

