﻿using System;

namespace Akka.TaskExec.Lib.Execution.Legacy
{
    public interface ITaskContext
    {            
        UInt64 TaskId { get; }
        UInt64 JobId { get; }
        Int32 JobNumber { get; }
        Int32 TaskNumber { get; }        
        UInt64 GroupId { get; }
        string TaskName { get; }
        string Description { get; }
        string TaskIdentifierString { get; }
		bool IsRestarted { get; }
		bool IsCancelled { get; }

		bool GetBooleanValue (string name, bool? defaultValue = null);
		string GetStringValue (string name, string defaultValue = null);
		int GetIntegerValue (string name, int? defaultValue = null);
		ulong GetUlongValue (string name, ulong? defaultValue = null);

		void ThrowIfCancellationRequested ();
		void UpdateProgress (string message, int progress);
    }
}
