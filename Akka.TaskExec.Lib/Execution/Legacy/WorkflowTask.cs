﻿namespace Akka.TaskExec.Lib.Execution.Legacy
{
	public class WorkflowTask
	{		
		protected ITaskContext _taskContext;
		ITaskContext TaskContext { get { return _taskContext; } }

	    public void Initialize(ITaskContext taskContext)
	    {
			_taskContext = taskContext;
	    }

        public virtual void GroupInitialize()
        {
        }

	    public virtual void GroupExecute()
		{
		}

		public virtual void Execute()
		{
		}

		public virtual void GroupFinalize()
		{
		}
	}
}

