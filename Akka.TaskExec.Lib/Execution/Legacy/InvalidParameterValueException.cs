﻿using System;

namespace Akka.TaskExec.Lib.Execution.Legacy
{
	/// <summary>
	/// The exception that is thrown when a parameter validation exception is encountered
	/// </summary>
	[Serializable]
	public class InvalidParameterValueException : Exception
	{
		public String ParameterName { get; private set; }
		public String ParameterValue { get; private set; }
		public Type ParameterType { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Wynyard.DataIngestion.Framework.Exceptions.InvalidParameterValueException"/> class.
		/// </summary>
		public InvalidParameterValueException(String message)
			: base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Wynyard.DataIngestion.Framework.Exceptions.InvalidParameterValueException"/>
		/// class with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.
		/// </param><param name="innerException">The exception that is the cause of the current exception. 
		/// If the <paramref name="innerException"/> parameter is not a null reference, the current exception is raised 
		/// in a catch block that handles the inner exception.
		/// </param>
		public InvalidParameterValueException(String message, Exception innerException)
			: base(message, innerException)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Wynyard.DataIngestion.Framework.Exceptions.InvalidParameterValueException"/>
		/// class with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="parameterName">Parameter Name</param>
		/// <param name="expectedType">Expected Type</param>
		/// <param name="parameterValue">Actual Value</param>
		/// <param name="additionalInfo">Optional additional information describing the problem</param>
		public InvalidParameterValueException(String parameterName, Type expectedType, String parameterValue, String additionalInfo = null)
			: base(String.Format("Parameter {0}: Expected to be of type {1} had an invalid value of '{2}'{3}", parameterName,
				expectedType.Name, parameterValue ?? String.Empty, String.IsNullOrWhiteSpace(additionalInfo) ? String.Empty : " (" + additionalInfo + ")"))
		{
			ParameterName = parameterName;
			ParameterValue = parameterValue;
			ParameterType = expectedType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Wynyard.DataIngestion.Framework.Exceptions.InvalidParameterValueException"/>
		/// class with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="parameterName">Parameter Name</param>
		/// <param name="expectedType">Expected Type, logical type i.e. "Directory", "File" etc.</param>
		/// <param name="parameterValue">Actual Value</param>
		/// <param name="additionalInfo">Optional additional information describing the problem</param>
		public InvalidParameterValueException(String parameterName, String expectedType, String parameterValue, String additionalInfo = null)
			: base(String.Format("Parameter {0}: Expected to be of type {1} had an invalid value of '{2}'{3}", parameterName,
				expectedType, parameterValue ?? String.Empty, String.IsNullOrWhiteSpace(additionalInfo) ? String.Empty : " (" + additionalInfo + ")"))
		{
			ParameterName = parameterName;
			ParameterValue = parameterValue;
			ParameterType = typeof(String);
		}
	}
}

