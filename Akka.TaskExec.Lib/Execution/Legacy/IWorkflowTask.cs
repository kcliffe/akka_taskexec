﻿using System;

namespace Akka.TaskExec.Lib.Execution.Legacy
{
    public interface IWorkflowTask : IDisposable
    {
        void Initialize(ITaskContext taskContext);

        void Execute();

        void GroupInitialize();

        void GroupFinalize();
    }
}
