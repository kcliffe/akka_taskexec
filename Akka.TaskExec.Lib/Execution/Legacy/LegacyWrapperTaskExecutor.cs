﻿using System;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Management;
using Akka.Event;

namespace Akka.TaskExec.Lib.Execution.Legacy
{
	public class LegacyWrapperTaskExecutor : BaseTaskExecutor
	{
		// For now. Should be IOC'd when we have that...
		public Type OrginalType { get; set; }

		readonly ILoggingAdapter Logger;

		public LegacyWrapperTaskExecutor (Signal signal, IManagementNotification mn, ILoggingAdapter Logger) : base(signal, mn)
		{
		}

		public override void Execute(TaskDefinition taskDefinition)
		{
			Logger.Debug ("Legacy task wrapper about to construct ITaskContext");

			var legacyTaskId = ulong.Parse (taskDefinition.Id);

			ITaskContext taskContext 
				= new TaskContext (legacyTaskId, (ulong)0, taskDefinition.Name, taskDefinition.Description, 
					taskDefinition.IsRestarted, taskDefinition.Params, Signal, Progress);

			Logger.Debug ("Legacy task wrapper has constructed ITaskContext and is calling the original task");

			var task = (WorkflowTask)Activator.CreateInstance(OrginalType);
			task.Initialize (taskContext);
			task.Execute ();
		}
	}
}

