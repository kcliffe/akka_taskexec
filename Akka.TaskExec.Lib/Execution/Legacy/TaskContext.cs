﻿using System;
using System.Collections.Generic;
using Akka.TaskExec.Lib.Management;

namespace Akka.TaskExec.Lib.Execution.Legacy
{
	public class TaskContext : ITaskContext
	{
		private readonly Signal _signal;
		private IManagementNotification _progress;

		public UInt64 TaskId { get; private set; }
		public UInt64 JobId { get; private set; }
		public Int32 JobNumber { get { throw new NotImplementedException (); } }
		public Int32 TaskNumber { get { throw new NotImplementedException (); } }
		public UInt64 GroupId { get { throw new NotImplementedException (); } }
		public string TaskName { get; private set; }
		public string Description { get; private set; }
		public string TaskIdentifierString { get; private set; }
		public bool IsRestarted { get; private set; }

		private readonly IDictionary<string, string> _parameters;

		public TaskContext (UInt64 taskId, UInt64 jobId, string taskName, string description,
			bool isRestarted, IDictionary<string, string> parameters, Signal signal, IManagementNotification progress)
		{
			TaskId = taskId;
			Description = description;
			JobId = jobId;
			TaskName = taskName;
			IsRestarted = isRestarted;

			_signal = signal;
			_progress = progress;

			// there is conversion code in the legacy ctor
			_parameters = parameters;

			//TaskIdentifierString = String.Format("{0}: ({1},{2})", TaskName, JobNumber, TaskNumber);
		}

		public bool IsCancelled
		{
			get 
			{
				return _signal.IsCancelled;
			}
		}

		public string GetStringValue(string name, string defaultValue = null)
		{
			string value;
			if (!_parameters.TryGetValue(name.ToLower(), out value))
			{
				if (defaultValue != null)
					return defaultValue;
			}

			return value ?? String.Empty;
		}

		public int GetIntegerValue(string name, int? defaultValue = null)
		{
			string stringValue;
			if (!_parameters.TryGetValue(name.ToLower(), out stringValue))
			{
				if (defaultValue != null)
					return (int)defaultValue;
			}

			int value;
			if (!int.TryParse(stringValue, out value))
				throw new InvalidParameterValueException(name, typeof(int), stringValue);

			return value;
		}

		public bool GetBooleanValue(string name, bool? defaultValue = null)
		{
			string stringValue;
			if (!_parameters.TryGetValue(name.ToLower(), out stringValue))
			{
				if (defaultValue != null)
					return (bool)defaultValue;
			}

			bool value;
			if (!bool.TryParse(stringValue, out value))
				throw new InvalidParameterValueException(name, typeof(bool), stringValue);

			return value;
		}

		public ulong GetUlongValue(string name, ulong? defaultValue = null)
		{
			string stringValue;
			if (!_parameters.TryGetValue(name.ToLower(), out stringValue))
			{
			    if (defaultValue != null)
			        return (ulong)defaultValue;
			}

			ulong value;
			if (!ulong.TryParse(stringValue, out value))
			    throw new InvalidParameterValueException(name, typeof(ulong), stringValue);

			return value;
		}

		public void ThrowIfCancellationRequested()
		{
			if (_signal.IsCancelled)
			{
				throw new Exception ();
			}
		}

		public void UpdateProgress(string message, int progressValue)
		{
			//_progress.Update (TaskId.ToString(), TaskName, progressValue, Consts.ProgressUpdate.InitialiseOnly, WorkUnitType.Absolute);
		}
	}
}

