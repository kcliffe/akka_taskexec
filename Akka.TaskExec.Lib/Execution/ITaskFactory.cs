﻿using Autofac;
using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Lib.Execution
{
    /// <summary>
    /// Types implementing this interface resolve TaskExecutors (default or legacy) 
    /// </summary>
	public interface ITaskFactory
	{
        TaskExecutorMetadata GetTaskImplementation(TaskDefinition message);
		void DoExecutorDiscovery(ContainerBuilder containerBuilder); // todo. can abstract autofac
	}
}

