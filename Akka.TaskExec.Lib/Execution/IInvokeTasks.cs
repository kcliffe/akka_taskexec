﻿using Akka.TaskExec.Lib.Serialize;

namespace Akka.TaskExec.Lib.Execution
{
	public interface IInvokeTasks
	{
		void Execute(TaskDefinition taskDefinition);
		void AddFilteredJob (string job);
	}
}

