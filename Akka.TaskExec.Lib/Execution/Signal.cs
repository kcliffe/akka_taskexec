﻿namespace Akka.TaskExec.Lib.Execution
{
	/// <summary>
	/// Instance which signifies an event a handler should know about.
	/// More doc required.
	/// </summary>
	public class Signal
	{
		public enum SignalType
		{
			Unset,
			Cancel, // cannot be restarted
			Abort,  // stop (but is restartable)
			Start   // start after cancel
		}

		private SignalType Set { get; set; }

		public bool IsSet 
		{ 
			get { 
				return Set == SignalType.Unset; 
			}
		}

		public bool IsCancelled
		{ 
			get { 
				return Set == SignalType.Cancel; 
			}
		}

		// Review this. it's currently the loop controller for the consumer. 
		// We *may* want to use to exit the loop - but currently we just close
		// the consuming process.....
		public bool ProcessMessages { get { return true; } }

		public Signal ()
		{
		}

		public void SetSignal(SignalType signalType)
		{
			Set = signalType;
		}
	}
}

