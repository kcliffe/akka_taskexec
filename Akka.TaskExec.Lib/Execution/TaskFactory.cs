﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using Autofac;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Execution.Legacy;

namespace Akka.TaskExec.Lib.Execution
{
	/// <summary>
	/// Return an implementation (instance of class) which can execute a given Task - a VERY POC attempt!                                   --------------------
	/// </summary>
	public class TaskFactory : ITaskFactory
	{
		// Cache handlers for each message type - obviously one per type currently
		protected static ConcurrentDictionary<string, Type> _handlersByMessageType;

		public TaskFactory()        
		{           
			_handlersByMessageType = new ConcurrentDictionary<string, Type> ();
		}

		public TaskExecutorMetadata GetTaskImplementation(TaskDefinition message) 
		{
			Type type;

			if (_handlersByMessageType.TryGetValue (message.Name+"Executor", out type)) {
				return new TaskExecutorMetadata(false, type);
			}

			if (_handlersByMessageType.TryGetValue (message.Name, out type)) {
				return new TaskExecutorMetadata(false, type);
			}

			// Logger.Error("Handler not found for type {0}", message.Name);
			// throw new NonTransientException(string.Format("Could not locate a handler(class) for message {0}", message.Name));
			throw new Exception(string.Format("Could not locate a handler(class) for message {0}", message.Name));
		}

		public void DoExecutorDiscovery(ContainerBuilder builder) 
		{
			var handlers = new List<Type> ();

			// default task executors
			handlers.AddRange(InternalDiscoverTypes ((t) => 
				{
					return typeof(BaseTaskExecutor).IsAssignableFrom(t);
				}));

			// legacy task executors
			handlers.AddRange(InternalDiscoverTypes ((t) =>  
				{
					return typeof(WorkflowTask).IsAssignableFrom(t);
				}));

			handlers.ForEach(x => 
				{ 
					_handlersByMessageType.TryAdd(x.Name, x); 
					builder.RegisterType(x).AsSelf().PropertiesAutowired(); // Maybe Peter can suss out why props seemingly not configured
				});
		}

		private IEnumerable<Type> InternalDiscoverTypes(Func<Type, bool> discoFunc)
		{
			var folderName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var assemblies = TypeEnumerator.EnumerateTypes(folderName, "Akka.TaskExec.*.dll", discoFunc);

			foreach (var type in assemblies)
			{
				yield return type;
				// Logger.Debug("Found handler of type {0}", type.Name);
			}
		}
	}

	public class TaskExecutorMetadata
	{
		private readonly bool _isLegacy;
		private readonly Type _handlerType;

		public bool IsLegacy
		{
			get { return _isLegacy; }
		}

		public Type HandlerType
		{
			get { return _handlerType; }
		}

		public TaskExecutorMetadata(bool isLegacy, Type handlerType)
		{
			_handlerType = handlerType;
			_isLegacy = isLegacy;
		}
	}
}

