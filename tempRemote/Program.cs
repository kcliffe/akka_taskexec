﻿using Akka.Actor;
using Akka.Configuration;
using System;
using Akka.TaskExec.Lib;
using Akka.TaskExec.Lib.Actors.Consumer;
using Akka.TaskExec.Lib.Actors.Producer;
using Akka.TaskExec.Lib.Messages;

namespace tempRemote
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = ConfigurationFactory.ParseString(@"
akka {
    actor {
        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
    }
    remote {
        helios.tcp {
            port = 8090
            hostname = localhost
        }
    }
}
");

            var system = ActorSystem.Create("MyClient", config);

            //get a reference to the producer actor
            var producer = system.ActorSelection("akka.tcp://myActorSystem@localhost:8080/user/conductor}");
            producer.Tell(new EnumerateClustersMsg());

            // create a worker actor
            var props = Props.Create<WorkerActor>(new Factory(), producer.Anchor);
            var worker = system.ActorOf(props, "workerEx");

            //get a reference to the remote actor
            var clusterQueue = system.ActorSelection("akka.tcp://myActorSystem@localhost:8080/user/conductor/default}");

            //send a message to the remote actor
            clusterQueue.Tell(new RegisterWorkerMsg("1", worker.Path.ToSerializationFormat()));

            Console.ReadLine();

        }
    }
}
