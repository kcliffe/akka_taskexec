﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Akka.TaskExec.Storage.Schema;
using Akka.TaskExec.Lib.Serialize;
using Akka.TaskExec.Lib.Storage;

namespace Akka.TaskExec.Storage
{
    public class MemoryStorage : IPersistJobs
    {
		// this doubles as our cache :-) All persisters should employ write-thru cache.
        private readonly ConcurrentDictionary<string, IPersistableJob> _jobs;
		private readonly ConcurrentDictionary<string, IPersistableTask> _tasks;
	
        public MemoryStorage()
        {
            _jobs = new ConcurrentDictionary<string, IPersistableJob>();
			_tasks = new ConcurrentDictionary<string, IPersistableTask> ();

			Mapper.CreateMap<IPersistableJob, JobDefinition> ();
			Mapper.CreateMap<JobDefinition, IPersistableJob> ();
			Mapper.CreateMap<IPersistableTask, TaskDefinition> ();
			Mapper.CreateMap<TaskDefinition, IPersistableTask> ();
        }

        public List<JobDefinition> GetAll()
		{
			try
			{
                return _jobs.Values.Select(m => Mapper.Map<JobDefinition>(m)).ToList();
			}
			catch(Exception ex)
			{
				return null;
			}
        }

        public JobDefinition GetById(string id)
        {
            IPersistableJob job;

            if (_jobs.TryGetValue(id, out job))            
                return Mapper.Map<JobDefinition>(job);

            throw new Exception(string.Format("Job {0} not found", id));
        }

        public JobDefinition Put(JobDefinition jobDefinition)
        {
            if (string.IsNullOrEmpty(jobDefinition.Id))
            {
				jobDefinition.Id = string.Format("{0}_{1}",jobDefinition.Id, DateTime.Now.ToFileTime());
            }

			var persistableJob = Mapper.Map<IPersistableJob>(jobDefinition);
			if (!_jobs.ContainsKey (jobDefinition.Id)) 
			{
				persistableJob.Tasks.ForEach(t => _tasks.TryAdd(t.Id, t));
			}

            _jobs[jobDefinition.Id] = persistableJob;
            
            return jobDefinition;
        }

		public void UpdateTask(TaskDefinition taskDefinition)
		{
			var persistableTask = _tasks[taskDefinition.Id];
			Mapper.Map (taskDefinition, persistableTask);
		}
    }
}